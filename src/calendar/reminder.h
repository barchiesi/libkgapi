/*
 * This file is part of LibKGAPI library
 *
 * Copyright (C) 2013  Daniel Vrátil <dvratil@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBKGAPI2_REMINDER_H
#define LIBKGAPI2_REMINDER_H

#include "object.h"
#include "types.h"
#include "kgapicalendar_export.h"

#include <KCalendarCore/Alarm>
#include <KCalendarCore/Incidence>

#include <QScopedPointer>

namespace KGAPI2
{

/**
 * @brief Represents a default calendar reminder.
 *
 * @author Daniel Vrátil <dvratil@redhat.com>
 * @since 0.4
 */
class KGAPICALENDAR_EXPORT Reminder
{
  public:

    /**
     * @brief Constructor
     */
    explicit Reminder();

    /**
     * @brief Constructor
     *
     * @param type Type of the reminder (email, notification, etc.)
     * @param startOffset How long before the event should the reminder be shown
     */
    explicit Reminder(const KCalendarCore::Alarm::Type &type,
                      const KCalendarCore::Duration &startOffset = KCalendarCore::Duration(0));

    /**
     * @brief Copy constructor
     */
    Reminder(const Reminder &other);

    /**
     * @brief Destructor
     */
    virtual ~Reminder();

    bool operator==(const Reminder &other) const;

    /**
     * @brief Returns type of the reminder
     */
    KCalendarCore::Alarm::Type type() const;

    /**
     * @brief Sets type of the reminder
     *
     * @param type
     */
    void setType(KCalendarCore::Alarm::Type type);

    /**
     * @brief Returns how long before the event should reminder be shown
     */
    KCalendarCore::Duration startOffset() const;

    /**
     * @brief Sets how long before the event should reminder be shown
     */
    void setStartOffset(const KCalendarCore::Duration &startOffset);

    /**
     * @brief Converts the reminder to a KCalendarCore::Alarm
     *
     * @param incidence An incidence on which the reminder should be applied
     * @return Returns a new KCalendarCore::Alarm
     */
    KCalendarCore::Alarm *toAlarm(KCalendarCore::Incidence *incidence) const;

  private:
    class Private;
    QScopedPointer<Private> const d;
};

} // namespace KGAPI2

#endif // LIBKGAPI2_REMINDER_H
